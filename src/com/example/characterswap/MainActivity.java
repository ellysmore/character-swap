package com.example.characterswap;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends Activity implements View.OnClickListener {

	private ImageView slotA, slotB, slotC;
	private RelativeLayout rLayout;
	private int layoutX, layoutY;
	private int swapDuration = 1000;
	private PointF translateAtoB, translateCtoA;
	private float grow, shrink;

	private static enum Position {
		A, B, C
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		slotA = (ImageView) findViewById(R.id.imageView1);
		slotA.setOnClickListener(this);
		slotB = (ImageView) findViewById(R.id.imageView2);
		slotB.setOnClickListener(this);
		slotC = (ImageView) (ImageView) findViewById(R.id.imageView3);
		slotC.setOnClickListener(this);
		rLayout = (RelativeLayout) findViewById(R.id.relativeLayout1);

	}

	public void calculationForSwaps() {
		Resources r = getResources();
		shrink = r.getDimension(R.dimen.unselected_char_height)
				/ r.getDimension(R.dimen.selected_char_height);
		grow = r.getDimension(R.dimen.selected_char_height)
				/ r.getDimension(R.dimen.unselected_char_height);
		translateAtoB = new PointF(layoutX / 12
				- (layoutX - r.getDimension(R.dimen.selected_char_width)) / 2,
				(layoutY - layoutY / 12)
						- (layoutY + r
								.getDimension(R.dimen.selected_char_height))
						/ 2);
		translateCtoA = new PointF(
				(layoutX + r.getDimension(R.dimen.selected_char_width)) / 2
						- (layoutX - layoutX / 12),
				(layoutY + r.getDimension(R.dimen.selected_char_height)) / 2
						- (layoutY - layoutY / 12));

	}

	private void alignChar(ImageView character, Position position) {
		RelativeLayout.LayoutParams params = null;

		layoutX = rLayout.getWidth();
		layoutY = rLayout.getHeight();

		switch (position) {
		case A:
			params = new RelativeLayout.LayoutParams((int) getResources()
					.getDimension(R.dimen.selected_char_width),
					(int) getResources().getDimension(
							R.dimen.selected_char_height));
			params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
			params.setMargins(0, 0, 0, 0);
			break;

		case B:
			params = new RelativeLayout.LayoutParams((int) getResources()
					.getDimension(R.dimen.unselected_char_width),
					(int) getResources().getDimension(
							R.dimen.unselected_char_height));
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
					RelativeLayout.TRUE);
			params.setMargins(layoutX / 12, 0, 0, layoutY / 12);

			break;

		case C:
			params = new RelativeLayout.LayoutParams((int) getResources()
					.getDimension(R.dimen.unselected_char_width),
					(int) getResources().getDimension(
							R.dimen.unselected_char_height));
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
					RelativeLayout.TRUE);
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
					RelativeLayout.TRUE);
			params.setMargins(0, 0, layoutX / 12, layoutY / 12);
			break;

		default:
			break;
		}
		character.setLayoutParams(params);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		alignChar(slotA, Position.A);
		alignChar(slotB, Position.B);
		alignChar(slotC, Position.C);
		calculationForSwaps();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.imageView2:
			rotateClockwise();
			break;
		case R.id.imageView3:
			rotateCounterClockwise();
			break;
		default:
			break;
		}

	}

	private void rotateClockwise() {
		AnimationSet animBtoA = new AnimationSet(true);
		animBtoA.setInterpolator(new BounceInterpolator());
		animBtoA.setFillAfter(true);

		Animation translationBtoA = new TranslateAnimation(0, -translateAtoB.x,
				0, -translateAtoB.y);
		translationBtoA.setDuration(swapDuration);
		Animation sizeBtoA = new ScaleAnimation(1f, grow, 1f, grow,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		sizeBtoA.setDuration(swapDuration);
		animBtoA.addAnimation(sizeBtoA);
		animBtoA.addAnimation(translationBtoA);

		Animation translationCtoB = new TranslateAnimation(0, slotB.getLeft()
				- slotC.getLeft(), 0, 0);
		translationCtoB.setDuration(swapDuration);
		translationCtoB.setInterpolator(new BounceInterpolator());
		translationCtoB.setFillAfter(true);

		AnimationSet animAtoC = new AnimationSet(true);
		animAtoC.setInterpolator(new BounceInterpolator());
		animAtoC.setFillAfter(true);

		Animation translationAtoC = new TranslateAnimation(0, -translateCtoA.x,
				0, -translateCtoA.y);
		translationAtoC.setDuration(swapDuration);
		Animation sizeAtoC = new ScaleAnimation(1f, shrink, 1f, shrink,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		sizeAtoC.setDuration(swapDuration);
		animAtoC.addAnimation(sizeAtoC);
		animAtoC.addAnimation(translationAtoC);

		animAtoC.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				slotA.setEnabled(false);
				slotB.setEnabled(false);
				slotC.setEnabled(false);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				slotA.setEnabled(true);
				slotB.setEnabled(true);
				slotC.setEnabled(true);

			}
		});
		slotA.startAnimation(animAtoC);
		slotB.startAnimation(animBtoA);
		slotC.startAnimation(translationCtoB);

	}

	private void rotateCounterClockwise() {
		AnimationSet animAtoB = new AnimationSet(true);
		animAtoB.setInterpolator(new BounceInterpolator());
		animAtoB.setFillAfter(true);

		Animation translationAtoB = new TranslateAnimation(0, translateAtoB.x,
				0, translateAtoB.y);
		translationAtoB.setDuration(swapDuration);
		Animation sizeAtoB = new ScaleAnimation(1f, shrink, 1f, shrink,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		sizeAtoB.setDuration(swapDuration);
		animAtoB.addAnimation(sizeAtoB);
		animAtoB.addAnimation(translationAtoB);

		Animation translationBtoC = new TranslateAnimation(0, slotC.getLeft()
				- slotB.getLeft(), 0, 0);
		translationBtoC.setDuration(swapDuration);
		translationBtoC.setInterpolator(new BounceInterpolator());
		translationBtoC.setFillAfter(true);

		AnimationSet animCtoA = new AnimationSet(true);
		animCtoA.setInterpolator(new BounceInterpolator());
		animCtoA.setFillAfter(true);

		Animation translationCtoA = new TranslateAnimation(0, translateCtoA.x,
				0, translateCtoA.y);
		translationCtoA.setDuration(swapDuration);
		Animation sizeCtoA = new ScaleAnimation(1f, grow, 1f, grow,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		sizeCtoA.setDuration(swapDuration);
		animCtoA.addAnimation(sizeCtoA);
		animCtoA.addAnimation(translationCtoA);

		animCtoA.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				slotA.setEnabled(false);
				slotB.setEnabled(false);
				slotC.setEnabled(false);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				slotA.setEnabled(true);
				slotB.setEnabled(true);
				slotC.setEnabled(true);

			}
		});
		slotA.startAnimation(animAtoB);
		slotB.startAnimation(translationBtoC);
		slotC.startAnimation(animCtoA);
	}

}
